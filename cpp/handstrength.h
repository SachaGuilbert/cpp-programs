#include <algorithm>
#include <array>
#ifndef HANDSTRENGTH_H
#define HANDSTRENGTH_H

float nopairs(std::array<int, 7> cards){
    double totalscore;
    for(int i =0;i<7;i++){
        //std::cout<<1.0e-10*cards[i]<<"\n";
        totalscore = totalscore + 1.0e-10*cards[i];
        //std::cout<< score<<"\n";
    }
    //std::cout<<"Totalscore for this hand:" << totalscore << "\n";
    return totalscore;
}
float handstrength(int hand[2], int tablecards[5]){
    std::array<int, 7> cards;
    cards[0]=hand[0];
    cards[1]=hand[1];
    cards[2]=tablecards[0];
    cards[3]=tablecards[1];
    cards[4]=tablecards[2];
    cards[5]=tablecards[3];
    cards[6]=tablecards[4];
    std::sort(cards.begin(), cards.end());
    for(int i=0;i<7;i++){
    //    std::cout << cards[i]<<"\n";
    }
    float score;
    score = nopairs(cards);
    return score;
}

#endif
