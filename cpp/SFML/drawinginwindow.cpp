#include <SFML/Graphics.hpp>
#include <chrono>
#include <thread>

int main(){
    using namespace std::this_thread; // sleep_for, sleep_until
    using namespace std::chrono; // nanoseconds, system_clock, seconds
    sf::RenderWindow window(sf::VideoMode(800, 600), "My window");
    while (window.isOpen()){
        sf::Event event;
        while (window.pollEvent(event)){
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
                window.close();
        }
        sleep_until(system_clock::now() + seconds(1));
        window.clear(sf::Color::Black);
        window.display();

        sleep_until(system_clock::now() + seconds(1));
        window.clear(sf::Color::Green);
        window.display();

        sleep_until(system_clock::now() + seconds(1));
        window.clear(sf::Color::Red);
        window.display();

        sleep_until(system_clock::now() + seconds(1));
        window.clear(sf::Color::Blue);
        window.display();

        sleep_until(system_clock::now() + seconds(1));
        window.clear(sf::Color::White);
        window.display();
    }
    return 0;
}
