This is the standard config for a new program:
```
#include <iostream>
int newvariable = 77;
using std::cout;
using std::cin;

int main() {
    for (int i = 0; i < 1; i++) {
        cout <<"Value of variable is " << newvariable;
    }
    return 0;
}
```
Save your program and run this command:
```g++ firstprogram.cpp ; ./a.out```
