#include <thread>
#include <iostream>

int main() {
    for(int i=1;i<1000;i++){
        std::cout.width(i); std::cout << "#" <<flush;
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
    return 0;
}
