#include<iostream>
#include<cstdlib>
#include<ctime>

int main(){
    int ammount = 20;
    int randnumbers[ammount];
    int max = 100;
    srand(time(0));
    for(int i = 0; i<ammount; i++) {
        randnumbers[i] = rand()%max;
    }
    int sortedarray[ammount];

    int locationofsmallest;

    for(int i = 0; i<ammount; i++) {
        int smallestnumber = max;
        for(int i = 0; i<ammount; i++) {
            if(randnumbers[i] < smallestnumber){
                smallestnumber = randnumbers[i];
                locationofsmallest = i;
            }
        }
        sortedarray[i]= randnumbers[locationofsmallest]; 
        randnumbers[locationofsmallest] = max;
    }
    for(int i = 0; i<ammount; i++) {
         std::cout << sortedarray[i]<<"\n";
    }
}
