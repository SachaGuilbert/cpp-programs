#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std;

int genrand(){
    int randnum = rand()%10;
//     cout<<randnum<<"\n";
    return randnum;
}

int main() {
    srand(time(0));
    ifstream myFile;
    myFile.open("DIS1y.csv");
    vector<string> data;
    while(myFile.good()){
        string line;
        getline(myFile, line, ',');
        data.push_back(line);
    }
    vector<int> hist;
    for(int i=0;i<data.size()-1;i++){
        if(i % 6 == 0 && i!= 0){
            string str = data[i+1];
            int num = stoi(str);
            hist.push_back(num);
        }
    }
    int investering = 10000;
    int konto = investering;
    int beholdning = 0;
    int status = 0;
    int kurtage;
        for(int i=0;i<hist.size();i++){
            int kurs = hist[i];
            if(i%10 == genrand() && status == 0){
                //køb
                beholdning = konto/hist[i];
                kurtage = beholdning * kurs;
                konto -= kurtage;
                cout<<"KØB:     Dag: "<<i<<", Beholdning: "<<beholdning<<", konto: "<<konto<<"\n";
                status = 1;
            }
            if(i%10 == genrand() && status == 1){
                //salg
                kurtage = beholdning*kurs;
                beholdning = 0;
                konto += kurtage;
                cout<<"SALG:    Dag: "<<i<<", Konto: "<<konto<<", Beholdning: "<<beholdning<<", Gevinst: "<< konto-investering<<"\n";
                status = 0;
            }
            
        }
    int kurs = hist[hist.size()];
    beholdning = konto/hist[hist.size()];
    kurtage = beholdning * kurs;
    konto -= kurtage;
    cout<<"Der er gået: "<<hist.size()<<" dage og din endelige gevindst er: "<<konto-investering<<"\n";
    return 0;
}
