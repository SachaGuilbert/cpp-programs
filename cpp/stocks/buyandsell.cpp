#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std;

int main() {
    ifstream myFile;
    myFile.open("DIS1y.csv");
    vector<string> data;
    while(myFile.good()){
        string line;
        getline(myFile, line, ',');
        data.push_back(line);
    }
    vector<int> hist;
    for(int i=0;i<data.size()-1;i++){
        if(i % 6 == 0 && i!= 0){
            string str = data[i+1];
            int num = stoi(str);
            hist.push_back(num);
//              cout<<num<<"\n";
        }
    }
    int investering = 10000;
    int konto = 10000;
    int beholdning = 0;
    cout<<"Investering = "<<investering<<"\n";
    for(int i=0;i<hist.size();i++){
        if(i == 0){
            //køb på første dag
            beholdning = konto/hist[i];
            int kurs = hist[i];
            int kurtage = beholdning * kurs;
            konto = konto - kurtage;
            cout<<"Dag: "<<i<<", Beholdning: "<<beholdning<<", Kurtage: "<<kurtage<<", Konto: "<<konto<<"\n";
        }
        if(i == hist.size()-1){
            //sælg på sidste dag
            int kurs = hist[i];
            konto = konto + beholdning*kurs;
            cout<<"Dag: "<<i<<", Konto efter salg: "<<konto<<", Gevinst: " <<konto-investering<<"\n";
        }
    }
    cout<<"Der er gået: "<<hist.size()<<" dage eller cirka: "<<hist.size()/365<<" år\n";
    return 0;
}
