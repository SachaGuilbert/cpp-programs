#include <iostream>
#include <string>
#include "handstrength.h"
using std::cout;
using std::cin;
const int numberofplayers = 3;
int playerhands [numberofplayers][2];
int tablecards [5];
bool stillgoing = true;
std::string choice;
enum GameState {
    STARTING,
    DEALT,//two cards dealt to each player
    FLOP,//first three community cards dealt on table
    TURN,//fourth community card dealt on table
    RIVER,//fifth community card dealt on table
    RAISING,//last chance to raise before the game ends
    DONE//games over
};
GameState gameState = STARTING;

int main() {
    srand(time(NULL));
    //Distributing 2 random cards to the players
    for(int i=0; i<numberofplayers; i++){
        cout << "Player " << i<<"\n";
        for(int j=0; j<numberofplayers; j++){
            playerhands[i][j]=rand()%52;
            cout << playerhands[i][j]/13 <<" "<< playerhands[i][j]%13 <<"\n";
        }
    }
    gameState = FLOP;
    while(gameState != DONE){
        std::getline (cin, choice);
        
        if(choice == "check" && gameState == FLOP) {
            cout << "\nchecked\n  ";
            for(int i=0;i<3;i++){
                tablecards[i]=rand()%52;
                cout << "\n"<<tablecards[i]/13 << " " << tablecards[i]%13 <<"\n";
            }
            gameState = TURN;
        } else if (choice == "fold") {
            cout << "folded\n";
            gameState = DONE;
        }else if(choice == "check" && gameState == TURN) {
            cout << "\nchecked\n  ";
            tablecards[3]=rand()%52;
            cout << "\n"<<(rand()%52)/13 << " " << (rand()%52)%13 <<"\n";
            gameState = RIVER;
        }else if(choice == "check" && gameState == RIVER) {
            cout << "\nchecked\n  ";
            tablecards[4]=rand()%52;
            cout << "\n"<<(rand()%52)/13 << " " << (rand()%52)%13 <<"\n";
            gameState = RAISING;
        }else if(choice == "scores" && gameState == RAISING) {
            cout << "\nShowing scores:\n";
            cout << "\nPlayer 0 score:\n"<<handstrength(playerhands[0],tablecards);
            cout << "\nPlayer 1 score:\n"<<handstrength(playerhands[1],tablecards);
            //gameState = RAISING;
        }else{
            cout << "Not a valid move\n";
        }
    }
    return 0;
}
